import React, { Component } from 'react';
import '../../assets/css/style.css'
import { Input, Button } from 'antd';
import { connect } from 'react-redux';
import { step5 } from '../../redux/actions/actions';

class FifthStep extends Component {
    
    state = {
        choice: 'POCHO',
        choice2: 'POCHO'
    };

    handleOptionChange = (changeEvent) => {
        this.setState({
            [changeEvent.target.name]: changeEvent.target.value,
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { choice, choice2 } = this.state;
        
        this.props.step5({ choice, choice2 });
    }

    render() {

        return (
            <div>
                <div className="home">
                    <h1>5. Оформление полиса ОСАГО </h1>
                    <h4>Выбор страховой компании:</h4>
                    <table>
                        <tr>
                            <td>image</td>
                            <td>POCHO</td>
                            <td>4, 543.21</td>
                            <td>
                                <Input type="radio" value="POCHO" name="choice"
                                    checked={this.state.choice === 'POCHO'}
                                    onChange={this.handleOptionChange} />
                            </td>
                            <td>
                                <label>
                                    <Input type="radio" value="POCHO" name="choice2"
                                        checked={this.state.choice2 === 'POCHO'}
                                        onChange={this.handleOptionChange} />
                                    ОСАГО
                                </label>
                                <label>
                                    <Input type="radio" value="POCHO" name="choice2"
                                        checked={this.state.choice2 === 'POCHO'}
                                        onChange={this.handleOptionChange} />
                                    Е-ОСАГО
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>PECO</td>
                            <td>4, 612.44</td>
                            <td>
                                <label>
                                    <Input type="radio" value="PECO" name="choice"
                                        checked={this.state.choice === 'PECO'}
                                        onChange={this.handleOptionChange} />
                                </label>
                            </td>
                            <td>
                                <label>
                                    <Input type="radio" value="PECO" name="choice2"
                                        checked={this.state.choice2 === 'PECO'}
                                        onChange={this.handleOptionChange} />
                                    ОСАГО
                                </label>
                                <label>
                                    <Input type="radio" value="PECO" name="choice2"
                                        checked={this.state.choice2 === 'PECO'}
                                        onChange={this.handleOptionChange} />
                                    Е-ОСАГО
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>ИНГОССТРАХ</td>
                            <td>4, 688.28</td>
                            <td>
                                <label>
                                    <Input type="radio" value="ИНГОССТРАХ" name="choice"
                                        checked={this.state.choice === 'ИНГОССТРАХ'}
                                        onChange={this.handleOptionChange} />
                                </label>
                            </td>
                            <td>
                                <label>
                                    <Input type="radio" value="ИНГОССТРАХ" name="choice2"
                                        checked={this.state.choice2 === 'ИНГОССТРАХ'}
                                        onChange={this.handleOptionChange} />
                                    ОСАГО
                                </label>
                                <label>
                                    <Input type="radio" value="ИНГОССТРАХ" name="choice2"
                                        checked={this.state.choice2 === 'ИНГОССТРАХ'}
                                        onChange={this.handleOptionChange} />
                                    Е-ОСАГО
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>СОГЛАСИЕ</td>
                            <td>4, 702.30</td>
                            <td>
                                <label>
                                    <Input type="radio" value="СОГЛАСИЕ" name="choice"
                                        checked={this.state.choice === 'СОГЛАСИЕ'}
                                        onChange={this.handleOptionChange} />
                                </label>
                            </td>
                            <td>
                                <label>
                                    <Input type="radio" value="СОГЛАСИЕ" name="choice2"
                                        checked={this.state.choice2 === 'СОГЛАСИЕ'}
                                        onChange={this.handleOptionChange} />
                                    ОСАГО
                                </label>
                                <label>
                                    <Input type="radio" value="СОГЛАСИЕ" name="choice2"
                                        checked={this.state.choice2 === 'СОГЛАСИЕ'}
                                        onChange={this.handleOptionChange} />
                                    Е-ОСАГО
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>РОСГОССТРАХ</td>
                            <td>4, 803.12</td>
                            <td>
                                <label>
                                    <Input type="radio" value="РОСГОССТРАХ" name="choice"
                                        checked={this.state.choice === 'РОСГОССТРАХ'}
                                        onChange={this.handleOptionChange} />
                                </label>
                            </td>
                            <td>
                                <label>
                                    <Input type="radio" value="РОСГОССТРАХ" name="choice2"
                                        checked={this.state.choice2 === 'РОСГОССТРАХ'}
                                        onChange={this.handleOptionChange} />
                                    ОСАГО
                                </label>
                                <label>
                                    <Input type="radio" value="РОСГОССТРАХ" name="choice2"
                                        checked={this.state.choice2 === 'РОСГОССТРАХ'}
                                        onChange={this.handleOptionChange} />
                                    Е-ОСАГО
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>СОГАЗ</td>
                            <td>4, 828.15</td>
                            <td>
                                <label>
                                    <Input type="radio" value="СОГАЗ" name="choice"
                                        checked={this.state.choice === 'СОГАЗ'}
                                        onChange={this.handleOptionChange} />
                                </label>
                            </td>
                            <td>
                                <label>
                                    <Input type="radio" value="СОГАЗ" name="choice2"
                                        checked={this.state.choice2 === 'СОГАЗ'}
                                        onChange={this.handleOptionChange} />
                                    ОСАГО
                                </label>
                                <label>
                                    <Input type="radio" value="СОГАЗ" name="choice2"
                                        checked={this.state.choice2 === 'СОГАЗ'}
                                        onChange={this.handleOptionChange} />
                                    Е-ОСАГО
                                </label>
                            </td>
                        </tr>
                    </table>

                    <h4>Расчетные коэфициенты по ОСАГО:</h4>
                    <table>
                        <tr>
                            <td>по територии использования</td>
                            <td>1.7</td>

                        </tr>
                        <tr>
                            <td>по мощности ТС</td>
                            <td>1.2</td>

                        </tr>
                        <tr>
                            <td>по периоду использования ТС</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>по сроку страхования ТС</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>по класу КБМ</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>по наличию прицепа</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>по возрасту и стажу</td>
                            <td>1</td>
                        </tr> <tr>
                            <td>по допуску лиц к упралению</td>
                            <td>1</td>
                        </tr> <tr>
                            <td>по грубым нарушениям</td>
                            <td>1</td>
                        </tr>
                    </table>
                    <div className="check">
                        <Button onClick={this.handleSubmit}>Сохранить</Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        step5: (payload) => dispatch(step5(payload)),
    }
};

export default connect(undefined, mapDispatchToProps)(FifthStep);