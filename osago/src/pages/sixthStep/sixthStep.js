import React, { Component } from 'react';
import { Button } from 'antd';
import '../../assets/css/style.css'

export default class SixthStep extends Component {

    render() {
                   
        return (
            <div className="home"> 
                <h1>6. Оплата полиса ОСАГО:</h1>
                <div className="div">
                    <div className="button">
                        <Button type="primary">ОНЛАЙН (картами VISA, MasterCard, МИР)</Button>
                        <Button type="primary">ОФФЛАЙН (платежная квитанция) НСС</Button>
                    </div>
                    <div className="button">
                        <Button type="primary">ПОДГОТОВКА СЧЕТА (для юридических лиц)</Button>
                        <Button type="primary">ОФФЛАЙН (платежная квитанция) БСО СК</Button>
                    </div>
                </div>
            </div>
        )
    }
}
