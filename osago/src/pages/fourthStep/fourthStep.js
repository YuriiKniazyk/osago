import React, { Component } from 'react';
import { Input, Button, Select } from 'antd';
import '../../assets/css/style.css'
import { connect } from 'react-redux';
import { step4 } from '../../redux/actions/actions';

const { Option } = Select;

class FourthStep extends Component {
    
    state = {
        trailer: 'no',
        rent: 'no',
        destruction: 'no'
    };

    handleOptionChange = (changeEvent) => {
        this.setState({
            [changeEvent.target.name]: changeEvent.target.value,
        });
    }

    handleChangeS = (value, name) => {
        this.setState({[name]: value });
    }
    
    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { commencementDate, period, purpose, trailer, rent, destruction } = this.state;
        
        this.props.step4({ commencementDate, period, purpose, trailer, rent, destruction });
    }

    render() {

        return (
            <div>
                <div className="home">
                    <h1>4. Условия использования:</h1>
                    <form>
                        <div className='data'>
                            <label>Дата начала действия полися</label>
                            <Input type="date" name="commencementDate" onChange={this.handleChange}/>
                        </div>

                        <div className='data'>
                            <label>Период использования авто:</label>
                            <Select style={{ width: 300 }} onChange={value => this.handleChangeS(value, 'period')} name="period">
                                <Option value="1 месяц">1 месяц</Option>
                                <Option value="2 месяца">2 месяца</Option>
                                <Option value="3 месяца">3 месяца</Option>
                                <Option value="4 месяца">4 месяца</Option>
                                <Option value="5 месяцев">5 месяцев</Option>
                                <Option value="6 месяцев">6 месяцев</Option>
                                <Option value="7 месяцев">7 месяцев</Option>
                                <Option value="8 месяцев">8 месяцев</Option>
                                <Option value="9 месяцев">9 месяцев</Option>
                                <Option value="10 месяцев">10 месяцев</Option>
                                <Option value="11 месяцев">11 месяцев</Option>
                                <Option value="12 месяцев">12 месяцев</Option>
                            </Select>
                        </div>

                        <div className='data'>
                            <label>Наличие прицепа:</label>
                            <div>
                                <label>
                                    <Input type="radio" value="no" name="trailer"
                                        checked={this.state.trailer === 'no'}
                                        onChange={this.handleOptionChange} />
                                    Нет
                                </label>
                                <label>
                                    <Input type="radio" value="yes" name="trailer"
                                        checked={this.state.trailer === 'yes'}
                                        onChange={this.handleOptionChange} />
                                    Да
                                </label>
                            </div>
                        </div>

                        <div className='data'>
                            <label>Сдается в аренду:</label>
                            <div>
                                <label>
                                    <Input type="radio" value="no" name="rent"
                                        checked={this.state.rent === 'no'}
                                        onChange={this.handleOptionChange} />
                                    Нет
                                </label>
                                <label>
                                    <Input type="radio" value="yes" name="rent"
                                        checked={this.state.rent === 'yes'}
                                        onChange={this.handleOptionChange} />
                                    Да
                                </label>
                            </div>
                        </div>

                        <div className='data'>
                            <label>Цель использования:</label>
                            <Select style={{ width: 300 }} onChange={value => this.handleChangeS(value, 'purpose')} name="purpose">
                                <Option value="личная">личная</Option>
                                <Option value="общественная">общественная</Option>
                                <Option value="оренда">оренда</Option>
                            </Select>
                        </div>

                        <div className='data'>
                            <label>Имелись ли грубые нарушения:</label>
                            <div>
                                <label>
                                    <Input type="radio" value="no" name="destruction"
                                        checked={this.state.destruction === 'no'}
                                        onChange={this.handleOptionChange} />
                                    Нет
                                </label>
                                <label>
                                    <Input type="radio" value="yes" name="destruction"
                                        checked={this.state.destruction === 'yes'}
                                        onChange={this.handleOptionChange} />
                                    Да
                                </label>
                            </div>
                        </div>
                    </form>
                    <div className="check">
                        <Button onClick={this.handleSubmit}>Сохранить</Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        step4: (payload) => dispatch(step4(payload)),
    }
};

export default connect(undefined, mapDispatchToProps)(FourthStep);