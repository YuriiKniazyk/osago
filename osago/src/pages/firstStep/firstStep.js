import React, { Component } from 'react';
import { Checkbox, Input, Select, Button } from 'antd';
import '../../assets/css/style.css'
import { connect } from 'react-redux';
import { step1 } from '../../redux/actions/actions';

const { Option } = Select;

class FirstStep extends Component {
    
    handleChangeS = (value, name) => {
        this.setState({[name]: value });
    }
    
    handleChange = (event) => {
        const {name, value} = event.target; 
        this.setState({
            [name]: value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { registrationAbroad, followsRegistration, typeVehicle, marka, model, power, documents, seriesDocuments, numberDocuments, dateOfIssue } = this.state;
        
        this.props.step1({ registrationAbroad: !registrationAbroad, followsRegistration: !followsRegistration, typeVehicle, marka, model, power, documents, seriesDocuments, numberDocuments, dateOfIssue });
    }

    render() {
    
        return (
            <div>
                <div className="home">
                    <h1>1. Транспортное средство:</h1>
                    <form>
                        <div className="check">
                            <Checkbox onChange={this.handleChange} name="registrationAbroad">Автомобиль зарегистрирован в иностранном государстве</Checkbox>
                        </div>
                        <div className="check">
                            <Checkbox onChange={this.handleChange} name="followsRegistration">Автомобиль следует к месту регистрации</Checkbox>
                        </div>

                        <div className='data'>
                            <label>Гос. номер авто:</label>
                            <Input type="text" name="avtoNumber" value={this.props.avtoNumber}/>
                        </div>

                        <div className='data'>
                            <label>Тип транспортного средства:</label>
                            <Select style={{ width: 300 }} onChange={(value)=>this.handleChangeS(value,'typeVehicle')} name="typeVehicle">
                                <Option value="jack">Jack</Option>
                                <Option value="В-Легковыеавтомобили">В - Легковые автомобили</Option>
                                <Option value="Yiminghe">yiminghe</Option>
                            </Select>
                        </div>

                        <div className='data'>
                            <label>VIN:</label>
                            <Input type="text" name='vin' placeholder='Z8NBCWJ32AS011864' value={this.props.vin}/>
                        </div>

                        <div className='data'>
                            <label>Марка:</label>
                            <Select style={{ width: 150 }} onChange={value => this.handleChangeS(value, "marka")} name="marka">
                                <Option value="АС">АС</Option>
                                <Option value="ВС">ВС</Option>
                                <Option value="КВ">КВ</Option>
                            </Select>
                        </div>

                        <div className='data'>
                            <label>Модель:</label>
                            <Select style={{ width: 150 }} onChange={value => this.handleChangeS(value, 'model')} name="model">
                                <Option value="Модель 1 1.8 RP">Модель 1 1.8 RP</Option>
                                <Option value="Модель 1 1.7 RP">Модель 1 1.7 RP</Option>
                                <Option value="Модель 1 1.9 RP">Модель 1 1.9 RP</Option>
                            </Select>
                        </div>

                        <div className='data'>
                            <label>Год выпуск:</label>
                            <Input type="text" name="yearAvto" value={this.props.yearAvto} />
                        </div>

                        <div className='data'>
                            <label>Мощность ТС (лс):</label>
                            <Select style={{ width: 250 }} onChange={value => this.handleChangeS(value, 'power')} name="power">
                                <Option value="до 50 лс включительно">до 50 лс включительно</Option>
                                <Option value="до 40 лс включительно">до 40 лс включительно</Option>
                                <Option value="до 30 лс включительно">до 30 лс включительно</Option>
                            </Select>
                        </div>

                        <div className='data'>
                            <label>Документы на ТС:</label>
                            <Select style={{ width: 250 }} onChange={value => this.handleChangeS(value, 'documents')} name="documents">
                                <Option value="Свидетельство о регистрации">Свидетельство о регистрации</Option>
                                <Option value="Паспорт">Паспорт</Option>
                                <Option value="Долар">Долар</Option>
                            </Select>
                        </div>

                        <div className='data'>
                            <label>Серия:</label>
                            <Input type="text" name="seriesDocuments" onChange={this.handleChange}/>
                        </div>

                        <div className='data'>
                            <label>Номер:</label>
                            <Input type="text" name="numberDocuments" onChange={this.handleChange}/>
                        </div>

                        <div className='data'>
                            <label>Дата выдачи:</label>
                            <Input type="date" name="dateOfIssue" onChange={this.handleChange}/>
                        </div>

                    </form>
                    <div className="check">
                        <Button onClick={this.handleSubmit}>Сохранить</Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        avtoNumber: state.avtoNumber,
        vin: state.vin,
        yearAvto: state.yearAvto,
        inspection: state.inspection, 
        registrationAbroad: state.registrationAbroad, 
        followsRegistration: state.followsRegistration, 
        typeVehicle: state.typeVehicle, 
        marka: state.marka, 
        model: state.model, 
        power: state.power, 
        documents: state.documents, 
        seriesDocuments: state.seriesDocuments, 
        numberDocuments: state.numberDocuments, 
        dateOfIssue: state.dateOfIssue
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        step1: (payload) => dispatch(step1(payload)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FirstStep);