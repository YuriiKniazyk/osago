import React, { Component } from 'react';
import '../../assets/css/style.css'
import { Checkbox, Input, Button } from 'antd';
import { connect } from 'react-redux';
import { home } from '../../redux/actions/actions';

class Home extends Component {
    
    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { name, birtday, passport, avtoNumber, vin, yearAvto, inspection } = this.state;

        this.props.home({ name, birtday, passport, avtoNumber, vin, yearAvto, inspection: !inspection });
    }

    render() {
                   
        return (
            <div>
                <div className='home'>
                    <h1>1. Расчет стоимости полиса ОСАГО</h1>
                    <form>
                        <div className='data'>
                            <label>ФИО:</label>
                            <Input type="text" name="name" placeholder='Иванов Дмитрий Петрович' onChange={this.handleChange}/>
                        </div>
                        <div className='data'>
                            <label>Дата рождения:</label>
                            <Input type="date" name='birtday' onChange={this.handleChange}/>
                        </div>
                        <div className='data'>
                            <label>Серия, номер вод.удостоверения::</label>
                            <Input type="text" name='passport' placeholder='7702 123456' onChange={this.handleChange}/>
                        </div>
                        <div className='data'>
                            <label>Гос. номер авто:</label>
                            <Input type="text" name="avtoNumber" placeholder='A 007 AA I 197' onChange={this.handleChange}/>
                        </div>
                        <div className='data'>
                            <label>VIN:</label>
                            <Input type="text" name='vin' placeholder='Z8NBCWJ32AS011864' onChange={this.handleChange}/>
                        </div>
                        <div className='data'>
                            <label>Год выпускa:</label>
                            <Input type="text" name="yearAvto" onChange={this.handleChange}/>
                        </div>
                        
                        <div className='check'>
                            <Checkbox onChange={this.handleChange} name="inspection">Проверка техосмотра</Checkbox>
                        </div>
                    </form>
                    <div className='check'>
                        <Button onClick={this.handleSubmit}>Сохранить</Button>
                    </div>            
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        name: state.name,
        birtday: state.birtday,
        passport: state.passport,
        avtoNumber: state.avtoNumber,
        vin: state.vin,
        yearAvto: state.yearAvto,
        inspection: state.inspection
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        home: (payload) => dispatch(home(payload)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);