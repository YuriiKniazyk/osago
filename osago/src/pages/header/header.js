import React from 'react';
import '../../assets/css/style.css';
import Start from '../home/home';
import FirstStep from '../firstStep/firstStep';
import SecondStep from '../secondStep/secondStep';
import ThirdStep from '../thirdStep/thirdStep';
import FourthdStep from '../fourthStep/fourthStep';
import FifthStep from '../fifthStep/fifthStep';
import SixthStep from '../sixthStep/sixthStep';
import { Steps, Button, message } from 'antd';

const { Step } = Steps;

const stepsum = [
    {
        title: 'Старт',
        description: 'Расчет стоимости',
        content: <Start />,
    },
    {
        title: 'Шаг 1',
        description: 'Расчет стоимости',
        content: <FirstStep />,
    },
    {
        title: 'Шаг 2',
        description: 'Страхователь',
        content: <SecondStep />,
    },
    {
        title: 'Шаг 3',
        description: 'Транспортное средство',
        content: <ThirdStep />,
    },
    {
        title: 'Шаг 4',
        description: 'Условия использования',
        content: <FourthdStep />,
    },
    {
        title: 'Шаг 5',
        description: 'Список водителей',
        content: <FifthStep />,
    },
    {
        title: 'Шаг 6',
        description: 'Оформление полиса ОСАГО',
        content: <SixthStep />,
    },
];
export default class Header extends React.Component {

    state = {
        current: 0,
    };

    onChange = current => {
        console.log('onChange:', current);
        this.setState({ current });
    };

    next() {
        const current = this.state.current + 1;
        this.setState({ current });
    }

    prev() {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    render() {
        const { current } = this.state;

        return (
            <div>
                <Steps current={current} onChange={this.onChange}>
                    {stepsum.map(item => (
                        <Step key={item.title} title={item.title} description={item.description} />
                    ))}
                </Steps>
                <div className="steps-content">{stepsum[current].content}</div>
                <div className="steps-action">
                    {current > 0 && (
                        <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>Назад</Button>
                    )}
                    {current < stepsum.length - 1 && (
                        <Button type="danger" onClick={() => this.next()}>Продолжить</Button>
                    )}
                    {current === stepsum.length - 1 && (
                        <Button type="danger" onClick={() => message.success('Processing complete!')}>Готово</Button>
                    )}
                </div>
            </div>
        )
    }
}