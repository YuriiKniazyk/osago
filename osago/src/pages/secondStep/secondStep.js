import React, { Component } from 'react';
import { Checkbox, Input, Select, Button} from 'antd';
import '../../assets/css/style.css'
import { connect } from 'react-redux';
import { step2 } from '../../redux/actions/actions';

const { Option } = Select;
class SecondStep extends Component {

    state = {
        radioInshur: 'fizMen',
    };

    handleOptionChange = (changeEvent) => {
        this.setState({
            radioInshur: changeEvent.target.value
        });
    }

    handleChangeS = (value, name) => {
        this.setState({[name]: value });
    }
    

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value,
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { identityDocument, seriesNumber, passportWhos, dateOfIssueDoc, address, isOwner, phone, email, radioInshur, kladr } = this.state;
        
        this.props.step2({ identityDocument, seriesNumber, passportWhos, dateOfIssueDoc, address, isOwner: !isOwner, phone, email, radioInshur, kladr });
    }

    render() {

        return (
            <div>
                <div className="home">
                    <h1>2. Страхователь:</h1>
                    <form>
                        <div className='data'>
                            <label>Страхователь ТС:</label>
                            <div className="check">
                                <label>
                                    <Input type="radio" value="fizMen"
                                        checked={this.state.radioInshur === 'fizMen'}
                                        onChange={this.handleOptionChange} />
                                    Физическое лицо
                                </label>

                                <label>
                                    <Input type="radio" value="urMen"
                                        checked={this.state.radioInshur === 'urMen'}
                                        onChange={this.handleOptionChange} />
                                    Юридическое лицо
                                </label>
                            </div>
                        </div>

                        <div className='data'>
                            <label>ФИО:</label>
                            <Input type="text" name="name" value={this.props.name}/>
                        </div>

                        <div className='data'>
                            <label>Дата рождения:</label>
                            <Input type="date" name='birtday' value={this.props.birtday}/>
                        </div>

                        <div className='data'>
                            <label>Документ, удостоверяющий личность:</label>
                            <Select style={{ width: 350 }} onChange={value => this.handleChangeS(value, 'identityDocument')} name="identityDocument">
                                <Option value="Паспорт гражданина РФ">Паспорт гражданина РФ</Option>
                                <Option value="Свидетельство о рождении">Свидетельство о рождении</Option>
                            </Select>
                            <Input type="text" name="seriesNumber" placeholder='серия, номер' onChange={this.handleChange}/>
                            <Input type="date" name="dateOfIssueDoc" placeholder='дата выдачи' onChange={this.handleChange}/>
                            <Input type="text" name="passportWhos" placeholder='кем выдан' onChange={this.handleChange}/>
                        </div>

                        <div className='data'>
                            <label>Адрес регистрации страхователя авто:</label>
                            <Input type="text" name="address" placeholder='Автоматический онлайн-подбор адреса' onChange={this.handleChange}/>
                        </div>

                        <div className='data'>
                            <label>КЛАДР:</label>
                            <Input type="text" name="kladr" placeholder='0205300000000' onChange={this.handleChange}/>
                        </div>

                        <div className="check">
                            <Checkbox onChange={this.onChange} name="isOwner">Страхователь является собственником</Checkbox>
                        </div>

                        <div className='data'>
                            <label>Контактный телефон:</label>
                            <Input type="text" name='phone' placeholder='8 (916) 3201799' onChange={this.handleChange}/>

                            <label>Email:</label>
                            <Input type="email" name='email' placeholder='post@gmail.com' onChange={this.handleChange}/>
                        </div>
                    </form>
                    <div className="check">
                        <Button onClick={this.handleSubmit}>Сохранить</Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        name: state.name,
        birtday: state.birtday,
        identityDocument: state.identityDocument,
        seriesNumber: state.seriesNumber,
        passportWhos: state.passportWhos,
        dateOfIssueDoc: state.dateOfIssueDoc,
        address: state.address,
        isOwner: state.isOwner,
        phone: state.phone,
        email: state.email,
        radioInshur: state.radioInshur,
        kladr: state.kladr
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        step2: (payload) => dispatch(step2(payload)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SecondStep);