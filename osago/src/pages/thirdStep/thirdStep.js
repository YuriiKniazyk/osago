import React, { Component } from 'react';
import { Input, Button } from 'antd';
import '../../assets/css/style.css'
import { connect } from 'react-redux';
import { step3 } from '../../redux/actions/actions';

class ThirdStep extends Component {
    
    state = {
        numberDriver: 'limited',
    };

    handleOptionChange = (changeEvent) => {
        this.setState({
            numberDriver: changeEvent.target.value
        });
    }

    handleChange = (event) => {
        const { name, value } = event.target;
        this.setState({
            [name]: value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        const { oneField, twoField, threeField, fourField, numberDriver } = this.state;

        this.props.step3({ oneField, twoField, threeField, fourField, numberDriver });
    }

    render() {

        return (
            <div>
                <div className="home">
                    <h1>3. Список водителей:</h1>
                    <form>
                        <div className='data'>
                            <label>Количество водителей:</label>
                            <div>
                                <label>
                                    <Input type="radio" value="limited"
                                        checked={this.state.numberDriver === 'limited'}
                                        onChange={this.handleOptionChange} />
                                    Ограничено
                                </label>

                                <label>
                                    <Input type="radio" value="unLimited"
                                        checked={this.state.numberDriver === 'unLimited'}
                                        onChange={this.handleOptionChange} />
                                    Не ограничено
                                </label>
                            </div>
                        </div>

                        <div className='data'>
                            <p>1.</p>
                            <Input type="text" name="oneField" onChange={this.handleChange}/>
                            <Input type="text" name="twoField" onChange={this.handleChange}/>
                            <Input type="text" name="threeField" onChange={this.handleChange}/>
                            <Input type="text" name="fourField" onChange={this.handleChange}/>
                        </div>
                        <label>+ Добавить водителя</label>

                    </form>
                    <div className="check">
                        <Button onClick={this.handleSubmit}>Сохранить</Button>
                    </div>
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        step3: (payload) => dispatch(step3(payload)),
    }
};

export default connect(undefined, mapDispatchToProps)(ThirdStep);