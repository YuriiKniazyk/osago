import React from 'react';
import { Button, } from "reactstrap";
import { Link, NavLink } from 'react-router-dom'

import styles from './styles/index.module.scss';

const MainButton = ({ onClick = () => { }, route = null, title = '', value=undefined,styleBut = {},...props }) => {
    if (route) {
        return <NavLink onClick={onClick} to={route} >
            <Button className={styles["main-button"]}>{title}</Button>
        </NavLink>
    } else {
        return (
            <Button onClick={onClick} className={styles["main-button"]} style={styleBut} >{title}</Button>
        )
    }

}

export default MainButton;