import { HOME, STEP1, STEP2, STEP3, STEP4, STEP5 } from "../action-types"

export const home = (payload) => {
    return {
        type: HOME,
        payload,
    }
}

export const step1 = (payload) => {
    return {
        type: STEP1,
        payload,
    }
}

export const step2 = (payload) => {
    return {
        type: STEP2,
        payload,
    }
}

export const step3 = (payload) => {
    return {
        type: STEP3,
        payload,
    }
}

export const step4 = (payload) => {
    return {
        type: STEP4,
        payload,
    }
}

export const step5 = (payload) => {
    return {
        type: STEP5,
        payload,
    }
}
