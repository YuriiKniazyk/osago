import { HOME, STEP1, STEP2, STEP3, STEP4, STEP5 } from "../action-types";

const initialState = {
    name: '',
    birtday: '',
    passport: '',
    avtoNumber: '',
    vin: '',
    yearAvto: '',
    inspection: false,
    registrationAbroad: false,
    followsRegistration: false,
    typeVehicle: '',
    marka: '',
    model: '',
    power: '',
    documents: '',
    seriesDocuments: '',
    numberDocuments: '',
    dateOfIssue: '',
    identityDocument: '',
    seriesNumber: '',
    passportWhos: '',
    dateOfIssueDoc: '',
    address: '',
    kladr: '',
    isOwner: false,
    phone: '',
    email: '',
    oneField: '', 
    twoField: '', 
    threeField: '', 
    fourField: '',
    commencementDate: '', 
    period: '', 
    purpose: '',
    radioInshur: '',
    numberDriver: '',
    trailer: '',
    destruction: '',
    rent:'',
    choice: '',
    choice2: '',
}; 

export default (state = initialState, action) => {
    const payload = action.payload;

    switch(action.type){
        case HOME: 
            return {...state, ...payload};

        case STEP1: 
            return {...state, ...payload};

        case STEP2: 
            return {...state, ...payload};
        
        case STEP3: 
            return {...state, ...payload};
        
        case STEP4: 
            return {...state, ...payload};
        
        case STEP5: 
            return {...state, ...payload};

        default: return state;
    }
};
  